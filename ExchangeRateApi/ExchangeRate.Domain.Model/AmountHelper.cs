﻿using System;

namespace ExchangeRate.Domain.Model
{
    public static class AmountHelper
    {
        public static decimal ConvertToPrecision(decimal? amount, int precision)
        {
            if (amount == null)
                return 0m;
            return Math.Round(amount.Value, precision);
        }
    }
}