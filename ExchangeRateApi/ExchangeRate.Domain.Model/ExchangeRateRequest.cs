﻿using System.ComponentModel.DataAnnotations;

namespace ExchangeRate.Domain.Model
{
    public class ExchangeRateRequest
    {
        [Required]
        [StringLength (3, ErrorMessage = "Base Currency Must be 3 Characters")]
        public string BaseCurrency { get; set; }

        [Required]
        [StringLength(3, ErrorMessage = "Target Currency Must be 3 Characters")]
        public string TargetCurrency { get; set; }
    }
}
