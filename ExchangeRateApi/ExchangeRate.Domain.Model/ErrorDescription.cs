﻿using Newtonsoft.Json;

namespace ExchangeRate.Domain.Model
{
    public class ErrorDescription
    {
        [JsonProperty("info")]
        public string ErrorMessage { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("code")]
        public int Code { get; set; }
    }
}