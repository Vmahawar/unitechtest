﻿using Newtonsoft.Json;
using System;

namespace ExchangeRate.Domain.Model
{
    public class ExchangeRateResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("timestamp")]
        public string TimeStamp { get; set; }

        [JsonProperty("base")]
        public string BaseCurrency { get; set; }

        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("rates")]
        public Rates ExchangeRate { get; set; }

        [JsonProperty("error")]
        public ErrorDescription Errors { get; set; }
    }

    public class Rates
    {
        private int precision = 5;

        private decimal audAmount, sekAmount, usdAmount, gbpAmount, eurAmount;

        public decimal AUD { get { return AmountHelper.ConvertToPrecision(audAmount, precision); } set { audAmount = value; } }
        public decimal SEK { get { return AmountHelper.ConvertToPrecision(sekAmount, precision); } set { sekAmount = value; } }
        public decimal USD { get { return AmountHelper.ConvertToPrecision(usdAmount, precision); } set { usdAmount = value; } }
        public decimal GBP { get { return AmountHelper.ConvertToPrecision(gbpAmount, precision); } set { gbpAmount = value; } }
        public decimal EUR { get { return AmountHelper.ConvertToPrecision(eurAmount, precision); } set { eurAmount = value; } }
    }
}