﻿using ExchangeRateService;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using Swashbuckle.AspNetCore.Swagger;
using ExchangeRateApi.Filters;
using Microsoft.Extensions.Logging;

namespace ExchangeRateApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(p=>p.Filters.Add(new ModelValidationFilter()));
            services.AddHttpClient();
            services.AddHttpClient<IFixerApiClient, FixerApiClient>(client =>
            {
                client.BaseAddress = new Uri("http://data.fixer.io/api/");
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.Timeout = new TimeSpan(0,0,5);
            });
          
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Exchange Rate API",
                    Description = "Exchange Rate API"
                });
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env,ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Exchange Rate API V1");
            });
           
            app.UseMvc();
        }
    }
}
