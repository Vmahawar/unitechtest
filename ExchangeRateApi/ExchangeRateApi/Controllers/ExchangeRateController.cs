﻿using ExchangeRate.Domain.Model;
using ExchangeRateService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace ExchangeRateApi.Controllers
{
    [Route("api/[controller]")]
    public class ExchangeRateController : Controller
    {
        private readonly IFixerApiClient _fixerApiClient;
        private readonly ILogger<ExchangeRateController> _logger;

        public ExchangeRateController(IFixerApiClient fixerApiClient, ILogger<ExchangeRateController> logger)
        {
            _fixerApiClient = fixerApiClient;
            _logger = logger;
        }

        [HttpGet("get/latest")]
        public async Task<ActionResult> GetLatestExchangeRateAsync([FromQuery] ExchangeRateRequest exchangeRateRequest)
        {
            try
            {
                _logger.LogInformation("Calling GetLatestExchangeRateAsync action");
                var result = await _fixerApiClient.GetLatestExchangeRateAsync(exchangeRateRequest).ConfigureAwait(false);
                return Ok(result);
            }
            catch (TaskCanceledException ex)
            {
                //Since we are not using any cancellation token for the task so it is safe to say it is timeout
                _logger.LogError("Timeout in GetLatestExchangeRateAsync action, Message {0}", ex.Message.ToString());
                return StatusCode(StatusCodes.Status408RequestTimeout);
            }
            catch (System.Exception ex)
            {
                _logger.LogError("Error in GetLatestExchangeRateAsync action, Message {0}", ex.Message.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError, "Something went wrong !!!");
            }
        }
    }
}
