﻿using ExchangeRate.Domain.Model;
using Microsoft.AspNetCore.WebUtilities;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ExchangeRateService
{
    public interface IFixerApiClient
    {
        Task<ExchangeRateResponse> GetLatestExchangeRateAsync(ExchangeRateRequest request);
    }

    public class FixerApiClient : IFixerApiClient
    {
        private readonly HttpClient _client;
        private readonly string _apiKey = "9e8330ef2020602e3ea952d8bc958886";
        private Dictionary<string, string> _query;

        public FixerApiClient(HttpClient httpClient)
        {
            _client = httpClient;
        }

        public async Task<ExchangeRateResponse> GetLatestExchangeRateAsync(ExchangeRateRequest request)
        {
            _query = new Dictionary<string, string>
            {
                ["access_key"] = _apiKey,
                ["from"] = request.BaseCurrency,
                ["to"] = request.TargetCurrency
            };

            var response = await _client.GetStringAsync(QueryHelpers.AddQueryString("latest", _query)).ConfigureAwait(false);
            return Newtonsoft.Json.JsonConvert.DeserializeObject<ExchangeRateResponse>(response);
        }
    }
}